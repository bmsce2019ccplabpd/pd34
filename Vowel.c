#include<stdio.h>
char input()
{
  char ch;
  printf("Enter a character to check if it is a vowel or not\n");
  scanf("%c",&ch);
  return ch;
}
int check(char ch)
{
  int k=0;
  switch(ch)
  {
    case 'A':
    case 'a': k++;
      break;
    case 'E':
    case 'e': k++;
      break;
    case 'I':
    case 'i': k++;
      break;
    case 'O':
    case 'o': k++;
      break;
    case 'U':
    case 'u': k++;
      break;
    default : k--;
  }
 return k; 
}
void display(char ch,int k)
{
   if(k>0)
  {
   printf("The given character %c is a vowel\n",ch);
  }
  else
  {
   printf("The given character %c is not a vowel\n",ch);
  }
}
int main()
{
  char ch;
  ch=input();
  int k=0;
  k=check(ch);
  display(ch,k);
  return 0;}
#include<stdio.h>
#include<stdlib.h>
struct employee
{
  char emp_name[20];
  char emp_id[20];
  char emp_dob[10];
  int age;
  int salary;
};

typedef struct employee EMPLOYEE;
EMPLOYEE input()
{
   EMPLOYEE e;	
   printf("\nEnter the name of the employee\n");
   scanf("%s",e.emp_name);
   printf("\nEnter the ID of the employee\n");
   scanf("%s",e.emp_id);
   printf("\nEnter the date of birth of the employee\n");
   scanf("%s",e.emp_dob);
   printf("\nEnter the age of the employee\n");
   scanf("%d",&e.age);
   printf("\nEnter the salary drawn by the employee\n");
   scanf("%d",&e.salary);
   return e;
}
void output(EMPLOYEE e)
{
  printf("The name of the employee is %s \n",e.emp_name);
  printf("The ID of the employee is %s \n",e.emp_id);
  printf("The date of birth of the employee is %s \n",e.emp_dob);
  printf("The age of the employee is :%d \n",e.age);
  printf("The salary drawn by the employee is :%d \n",e.salary);
}
int main()
{
  EMPLOYEE e;
  e=input();
  output(e);
  return 0;}
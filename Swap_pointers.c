#include<stdio.h>
void  swap(int*,int*);
int main()
{   int a=3,b=4;
    printf("In main,a=%d and b=%d",a,b);
    swap(&a,&b);
    return 0;
}
    void swap(int*c,int*d)
{ 
    int temp;
    temp=*c;
    *c=*d;
    *d=temp;
    printf("In fun(call_by_ref) a=%d and b=%d",*c,*d);
}